//doi tuong `validator
function Validator(options) {
  //hàm thực hiện validate
  function validate(inputElement, rule) {
    var errorMessage = rule.test(inputElement.value);
    var errorElement =
      inputElement.parentElement.querySelector(".form-message");

    if (errorMessage) {
      errorElement.innerText = errorMessage;
      inputElement.parentElement.classList.add("isvalid");
    } else {
      errorElement.innerText = "";
      inputElement.parentElement.classList.remove("isvalid");
    }
  }

  //lấy element của form cần validate
  var formElement = document.querySelector(options.form);

  if (formElement) {
    formElement.onsubmit = function (e) {
      e.preventDefault();
      options.rules.forEach(function (rule) {
        //lặp qua từng rule va
        var inputElement = formElement.querySelector(rule.selector);
        validate(inputElement, rule);
      });
    };
    // xử lý lặp qua mỗi rule và xử lý( lắng nghe sự kiện blủ, input,...)
    options.rules.forEach(function (rule) {
      var inputElement = formElement.querySelector(rule.selector);

      if (inputElement) {
        //xử lý trường hợp blur ra khỏi input
        inputElement.onblur = function () {
          validate(inputElement, rule);
        };
        // xử lý mỗi khi người dùng nhập vào input
        inputElement.oninput = function () {
          var errorElement =
            inputElement.parentElement.querySelector(".form-message");
          errorElement.innerText = "";
          inputElement.parentElement.classList.remove("isvalid");
        };
      }
    });
  }
}
//dinh nghia rules
// nguyên tắc của rule:
//1. khi có lỗi=> trả ra messae lỗi
//khi hợp lệ => không trả ra cái gì cả(indefined)
Validator.isRequired = function (selector) {
  return {
    selector: selector,
    test: function (value) {
      //trim() loại bỏ dấu cách
      return value.trim() ? undefined : "Không được để trống";
    },
  };
};
Validator.isNumber = function (selector) {
  return {
    selector: selector,
    test: function (value) {
      var regex = /^(0|[1-9]\d*)$/;
      return regex.test(value) ? undefined : "Xin vui lòng nhập số";
    },
  };
};
Validator.isImage = function (selector) {
  return {
    selector: selector,
    test: function (value) {
      //trim() loại bỏ dấu cách
      return value.trim()
        ? undefined
        : "Không được để trống, xin nhập link hình ảnh của sản phẩm";
    },
  };
};
