const CAP_URL = "https://637907167419b414df88bbab.mockapi.io";

var idEdited = null;
//Render all
function fetchall() {
  axios({
    url: `${CAP_URL}/capstonejs`,
    method: "GET",
  })
    .then(function (res) {
      renderCapstonejs(res.data);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
//chay lan dau
fetchall();
//nut xoa
function remove(idcapstonejs) {
  axios({
    url: `${CAP_URL}/capstonejs/${idcapstonejs}`,
    method: "DELETE",
  })
    .then(function (res) {
      //goi lai api
      fetchall();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
function addSP() {
  // check

  //
  var name = document.getElementById("tenSP").value;
  var price = document.getElementById("giaSP").value;
  var img = document.getElementById("hinhSP").value;
  var text = document.getElementById("motaSP").value;

  var newcapstone = {
    name: name,
    price: price,
    img: img,
    text: text,
  };

  axios({
    url: `${CAP_URL}/capstonejs`,
    method: "POST",
    data: newcapstone,
  })
    .then(function (res) {
      //goi lai api
      fetchall();
      Validator();
      document.querySelector("#myModal .close").click();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
// xem Sp
function editSP(idcapstonejs) {
  axios({
    url: `${CAP_URL}/capstonejs/${idcapstonejs}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res.data);

      //Mo modal
      //dua thong tin len form
      document.getElementById("tenSP2").value = res.data.name;
      document.getElementById("giaSP2").value = res.data.price;
      document.getElementById("hinhSP2").value = res.data.img;
      document.getElementById("motaSP2").value = res.data.text;

      idEdited = res.data.id;
    })
    .catch(function (err) {
      console.log(err);
    });
}
//update

function capnhatSP() {
  var data = layThongTinTuForm2();
  console.log("data: ", data);
  axios({
    url: `${CAP_URL}/capstonejs/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      console.log("res: ", res);
      fetchall();
      document.querySelector("#myModal2 .close").click();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}
//nut reset
function myFunction() {
  document.getElementById("myForm").reset();
}
